const workTabs = [...document.querySelectorAll('.work_tabs')];
const workTabsTitle = [...document.querySelectorAll('.work_tabs_title')];
const workTabsContent = [...document.querySelectorAll('.work_tabs_content')];
const workTabsItem = [...document.querySelectorAll('.tabs_item')];
const workTabsContentItemTitle = [...document.querySelectorAll('.work_tabs_content h3')];
const loadBtn = document.getElementById('loadMoreBtn');

let TabsItemNumber = 12;

showElemrnts(TabsItemNumber);

function showElemrnts(number) {
	workTabs.forEach((element) => {
		//console.log(element.children); // !!! это тольтко коллеко HTMLCollection, метод forEach для неё не работает
		// Каждому дочернему элементу массива [...element.children] добавляю dataset.attr с текстом элемента
		[...element.children].forEach(li => li.dataset.attr = li.innerText);

		workTabsItem.slice(0, number).forEach((item, num) => {
			// Добавляю каждому workTabsItem (от 1 до 12) dataset.attr с текстом элемента
			item.dataset.attr = workTabsContentItemTitle[num].textContent;
			// Изначально добавляю каждому workTabsItem (от 1 до 12) class="content_active" так, как таб All акткивный
			item.classList.add('content_active');
			document.querySelector('.work_tabs').classList.remove('active_tab');

			element.addEventListener('click', (event) => {
				//!!!Проверили, что кликнули на work_tabs_title (при выборе двух табов все ломается)
				if (event.target.classList.contains('work_tabs_title')) {
					// Находим work_tabs, у которого сейчас class="ve_tab" и удаляем этот класс,
					document.querySelector('.work_tabs .active_tab').classList.remove('active_tab');
					// а тому tab-у, по которму кликнули добавляем class="active_tab"
					event.target.classList.add('active_tab');
					// При клике на таб каждому workTabsItem  удаляю class="content_active"
					item.classList.remove('content_active');
					//Проетяем равенство <li class="work_tabs_title" > и <li class="tabs_item"> по аттрибутам
					if (event.target.dataset.attr == item.dataset.attr || event.target.dataset.attr == "All") {
						item.classList.add('content_active');
					}
				}
				evTarget = event.target.dataset.attr;
			})
		})
	})
	if (number >= 36) {
		loadBtn.style.display = 'none';
	}
}

loadBtn.addEventListener("click", () => {
	document.getElementById('spinner').style.opacity = 1;
	setTimeout(loaderMoreItems, 1500);
});

function loaderMoreItems() {
	document.getElementById('spinner').style.opacity = 0;
	TabsItemNumber += 12;
	loadBtn.style.backgroundColor = "var(--main-active-color)";
	showElemrnts(TabsItemNumber);
	// Имитирую клик по активному табу после load more
	document.getElementById(`${evTarget}`).click();
}




