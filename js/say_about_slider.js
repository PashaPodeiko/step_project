document.addEventListener('DOMContentLoaded', function () {
	// инициализация слайдера
	let slider = new SimpleAdaptiveSlider('.slider', {
		loop: true,
		autoplay: false,
		swipe: true
	});

	document.addEventListener('click', function (e) {
		let $target = e.target;
		if (!$target.classList.contains('btn-close')) {
			return;
		}
		let $container = document.querySelector('.slider-overflow__container');
		$container.classList.remove('slider-overflow__container_show');
	});

	let thumbnailsItem = document.querySelectorAll('.slider__thumbnails-item');
	thumbnailsItem.forEach(function ($item, index) {
		$item.dataset.slideTo = index;
	})

	function setActiveThumbnail() {
		let sliderItemActive = document.querySelector('.slider__item_active');
		let index = parseInt(sliderItemActive.dataset.index);
		for (let i = 0, length = thumbnailsItem.length; i < length; i++) {
			if (i !== index) {
				thumbnailsItem[i].classList.remove('active');
			} else {
				thumbnailsItem[index].classList.add('active');
			}
		}
	}
	setActiveThumbnail();
	document.querySelector('.slider').addEventListener('slider.set.active', setActiveThumbnail);
	let sliderThumbnails = document.querySelector('.slider__thumbnails');
	sliderThumbnails.addEventListener('click', function (e) {
		$target = e.target.closest('.slider__thumbnails-item');
		if (!$target) {
			return;
		}
		let index = parseInt($target.dataset.slideTo, 10);
		slider.moveTo(index);
	});
});
