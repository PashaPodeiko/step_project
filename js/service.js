const serviceTabsTitleArry = [...document.querySelectorAll('.service_tabs_title')];
const serviceTabsContent = [...document.querySelectorAll('.service_tabs_content li')];

serviceTabsTitleArry.forEach((el) => el.dataset.attr = el.textContent);

serviceTabsContent.forEach((item, position) => {
	item.dataset.attr = serviceTabsTitleArry[position].dataset.attr
});
/*
 !!! Недостаток решения - position. В HTML файле service_tabs_content должен сответствовать порядку service_tabs_title
*/
document.querySelector('.service_tabs').addEventListener('click', (event) => {
	//Проверили, что кликнули на service_tabs_title (при выборе двух табов все ломается)
	if (event.target.classList.contains('service_tabs_title')) {
		// Находим service_tabs_title, у которого сейчас class="active_bg with_triangle" и удаляем этот класс,
		document.querySelector('.service_tabs .active_bg, .with_triangle').classList.remove('active_bg', 'with_triangle');
		// а тому tab-у, по которму кликнули добавляем class="active_bg with_triangle"
		event.target.classList.add('active_bg', 'with_triangle');
		// Удаляем class="active" контенту у которого он есть
		document.querySelector('.service_tabs_content .content_active').classList.remove('content_active');
		// Добавляем class="active" контенту с таким же атрибутом
		document.querySelector(
			`.service_tabs_content li[data-attr="${event.target.dataset.attr}"]`
		).classList.add('content_active');
	}
});




